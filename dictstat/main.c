//Nadiia Chepurko nc409

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dictstat.h"


struct node{
    int numOfOccur; //number of occurences of that word in datafile. To be used to count exact words and prefixes
    int numOfSuperwords; //indicates number of superwords for the given word
    int isWord; //indicated if current character is the end of the dictionary word, 1 - indicates word, 0 - not a word.
    int *usedIndices; //front pointer to array of int indices
    int isParent;
    struct node *children; //front pointer
};

NodePtr root;

int charToIndex(char ch){
    if(ch >= 'a' && ch <= 'z'){
        return ch - 'a';
    }else if(ch >= 'A' && ch <= 'Z'){
        return ch - 'A';
    }else{
        return 26; //not an alpha char
    }
}

//void readDict(FILE *dict file): This function takes in a file pointer to the dictionary file and
//reads and stores words from the dictionary file into an appropriate data structure

void readDict(FILE *dict_file){
    
    root.numOfOccur = 0;
    root.numOfSuperwords = 0;
    root.isWord = 0;
    root.children = calloc(27, sizeof(struct node));
    root.usedIndices = calloc(27, sizeof(int));
    root.isParent = 0;
    
    NodePtr* temp = &root;
    
    int letterBefore = 0;
    int c = 0;
    int cFlag = 0;
    
    while((c = getc(dict_file)) != EOF){
        
        if((c = charToIndex(c)) <= 25 && c >= 0){
            if (temp -> children[c].children == NULL){
                temp -> children[c].children = calloc(27, sizeof(struct node));
                temp -> children[c].numOfOccur = 0;
                temp -> children[c].numOfSuperwords = 0;
                temp -> children[c].isWord = 0;
                temp -> children[c].usedIndices = calloc(27, sizeof(int));
                temp -> usedIndices[c] = 1;
                temp -> isParent = 1;
                temp = &temp -> children[c];
                letterBefore = 1;
                cFlag = 1;
            }else{
                temp = &temp -> children[c];
                letterBefore = 1;
                cFlag = 1;
            }
        }else if (letterBefore){
            temp -> isWord = 1; //last character indicates that it was the end of the word
            temp = &root;
            letterBefore = 0;
            cFlag = 0;
        }
    }
    if(cFlag == 1){
        temp -> isWord = 1;
    }
}

void updateTrie(char *str){
    
    NodePtr* temp = &root;
    int c = -1;
    while (*str) {
        c = charToIndex(*str);
        if(temp -> children[c].children != NULL){ // should we proceed to the next character ?
            if(temp -> isWord == 1)
                temp -> numOfSuperwords++;
            temp = &temp -> children[c];
        }else{//we reached the end point of a trie branch
            temp -> numOfSuperwords++;
            break;
        }
        str++;
    }
    if(!(*str)) temp -> numOfOccur++;
    
}

//void scanData(FILE *data file): This function takes in a file pointer to the data file and for
//every dictionary word stored in the data structure counts occurrences, prefixes and superwords in
//the data file.

void scanData(FILE *data_file){
    
    char *bufferPtr;
    int c = 0;
    
    while(c != EOF && (c = getc(data_file)) != EOF){
        if((c = charToIndex(c)) <= 25 && c >= 0){
            char *buffer = calloc(101, sizeof(char));
            bufferPtr = buffer;
            while(c <= 25 && c >= 0){
                char ch = 'a' + c; //or can use tolower(int c) function
                *bufferPtr = ch;
                bufferPtr++;
                c = getc(data_file);
                c = charToIndex(c);
            }
            *bufferPtr = '\0';
            updateTrie(buffer);
            free(buffer);
        }
    }
    
}


void recPrint(char *buffer, char *freeCh, NodePtr* temp, int prefixes){
    int occur = temp -> numOfOccur;
    int superWords = temp -> numOfSuperwords;
    if(temp -> isParent == 0){
        printf("%s %d %d %d\n", buffer, occur, prefixes, superWords);
        return;
    }
    
    if(temp -> isWord) printf("%s %d %d %d\n", buffer, occur, prefixes, superWords);
    
    prefixes += temp -> numOfOccur;
    int *intArr = temp -> usedIndices;
    int i = 0;
    for(i = 0; i < 26; i++){
        if(intArr[i] == 1){
            *freeCh = i + 'a';
            freeCh++;
            recPrint(buffer, freeCh, &temp -> children[i], prefixes);
            //now we need to remove all extra characters till the next turn
            freeCh--;
            *freeCh = '\0';
        }
    }
    
}

void printTrie(){
    
    if(root.isParent == 0){
        printf("empty dictionary\n");
    }
    char *buffer;
    char *freeCh;
    int *intArr = root.usedIndices;
    int prefixes = 0;
    int i = 0;

    for(i = 0; i < 26; i++){
        if(intArr[i] == 1){
            buffer = calloc(101, sizeof(char));
            freeCh = buffer;
            *freeCh = i + 'a';
            freeCh++;
            recPrint(buffer, freeCh, &root.children[i], prefixes);
            free(buffer);
        }
    }
}


//The program dictstat should have the following usage interface:
//dictstat <argument1> <argument2>
//where <argument1> is the name of the dictionary file, and <argument2> is the name of the data file.

int main(int argc, const char * argv[])
{
    if(argc != 3){
        fprintf(stderr, "invalid input\n");
        return 1;
    }
    
    FILE *dictFilePtr = fopen(argv[1], "r");
    if(dictFilePtr == NULL) {
      fprintf(stderr, "Cannot open the dictionary file\n");
      return 1;
    }
    
    FILE *dataFilePtr = fopen(argv[2], "r");
    if(dataFilePtr == NULL) {
        fprintf(stderr, "Cannot open the data file\n");
        return 1;
    }
    
    readDict(dictFilePtr);
    fclose(dictFilePtr);
    scanData(dataFilePtr);
    fclose(dataFilePtr);
    
    printTrie();
    
    return 0;
}

