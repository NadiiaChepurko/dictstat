#include <stdio.h>
#include <stdlib.h>

//definition for trie node
typedef struct node NodePtr;

//takes char value and converts to index representation of a char (0 - 25)
//returns 26 for non-alpabetic value
int charToIndex(char);

//takes a word from data file and update trie information for this word
void updateTrie(char *);

//This function takes in a file pointer to the dictionary file and
//reads and stores words from the dictionary file into an appropriate data structure.
void readDict(FILE *);

//This function takes in a file pointer to the data file and for
//every dictionary word stored in the data structure counts occurrences, prefixes and superwords in
//the data file.
void scanData(FILE *);


//Initialize data to print trie
void printTrie();

//Is used by printTrie() to print recursively words for each branch of root node
void recPrint(char *, char *, NodePtr*, int);









